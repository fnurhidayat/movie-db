const User = require('../models/user.js')
const jwt = require('jsonwebtoken')
const mailer = require('../middlewares/mailer.js')
const forgotPasswordHTML = require('../mailers/forgotPassword.js')

exports.register = async (req, res, next) => {
  try {
    req.body = {
      status: true,
      statusCode: 200,
      data: await User.create(req.body)
    }
  }

  catch(err) {
    req.body = {
      status: false,
      statusCode: 400,
      errors: err.message
    }
  }

  next()
}

exports.login = async (req, res, next) => {
  try {
    req.body = {
      status: true,
      statusCode: 200,
      data: await User.authenticate(req.body)
    }
  }

  catch(err) {
    req.body = {
      status: false,
      statusCode: 400,
      errors: err.message
    }
  }

  next()
}

exports.verify = async (req, res) => {
  try {
    let payload = await jwt.verify(req.query.verification_token, process.env.VERIFICATION_SECRET)
    let result = await User.verify(payload._id)
    return res.status(301).redirect(`${process.env.CLIENT_URL}?success=true&message=${result}`)
  }

  catch(err) {
    return res.status(307).redirect(`${process.env.CLIENT_URL}?success=false&message=${err.message}`)
  }
}

exports.forgotPassword = async (req, res, next) => {
  try {
    let token = await User.generateForgotPasswordToken(req.body.email)
    const html = forgotPasswordHTML({
      firstname: req.body.email,
      resetPasswordLink: `${process.env.CLIENT_URL}/reset-password?token=${token}`
    })

    /* istanbul ignore next */
    if (process.env.NODE_ENV !== 'test')
      await mailer.send({
        from: 'mailer@moviedb.io',
        to: req.body.email,
        subject: 'Reset Password Request',
        html
      });

    req.body = {
      status: true,
      statusCode: 201,
      data: 'Your request has sent, please check your email to reset your password!' 
    }
  }

  catch(err) {
    req.body = {
      status: false,
      statusCode: 400,
      errors: err.message
    }
  }

  next()
}

exports.resetPassword = async (req, res, next) => {
  try {
    let payload = await jwt.verify(req.query.token, process.env.FORGOT_PASSWORD_SECRET)
    req.body = {
      status: true,
      statusCode: 200,
      data: await User.resetPassword({
        ...req.body,
        _id: payload._id
      })
    }
  }

  catch(err) {
    req.body = {
      status: false,
      statusCode: 400,
      errors: err.message
    } 
  }

  next()
}

exports.me = async (req, res, next) => {
  req.body = {
    status: true,
    statusCode: 200,
    data: await User
      .findByIdWithDetail(req.headers.authorization._id)
  }

  next()
}
