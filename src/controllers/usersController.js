const UserDetail = require('../models/userDetail.js')

exports.update = async (req, res, next) => {
  await UserDetail
    .findOneAndUpdate({ user_id: req.headers.authorization._id }, req.body)

  req.body = {
    status: true,
    statusCode: 200,
    data: req.body
  }

  next()
}
