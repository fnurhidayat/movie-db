const express = require('express');
const morgan = require('morgan');
const app = express();
/* istanbul ignore next */
const env = process.env.NODE_ENV || 'development';

require('./adapter.js')

/* istanbul ignore next */
if (env !== 'production') require('dotenv').config();
/* istanbul ignore next */
if (env !== 'test') app.use(morgan('tiny'));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Root Endpoint
app.get('/', (_, res) => {
  return res.status(200).json({
    status: true,
    data: {
      message: 'Hello World',
      environment: env
    }
  })
})

const router = require('./router')
const present = require('./middlewares/present.js')
app.use('/api/v1', router, present)

const exception = require('./middlewares/exception.js')
exception.forEach(i => app.use(i))

module.exports = app;
