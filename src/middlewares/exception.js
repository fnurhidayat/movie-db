module.exports = [
  /* istanbul ignore next */
  function(err, req, res, next) {
    res.status(500).json({
      status: false,
      statusCode: 500,
      errors: err.message
    })
  },

  function(req, res) {
    res.status(404).json({
      status: false,
      statusCode: 404,
      errors: 'Not found!'
    }) 
  }
]
