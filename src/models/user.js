const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Schema = mongoose.Schema
const RegisterHTML = require('../mailers/register.js')
const mailer = require('../middlewares/mailer.js')
const UserDetail = require('./userDetail.js')

const userSchema = new Schema({
  username: {
    type: 'string',
    optional: true,
		lowercase: true
  },
  email: {
    type: 'string',
    required: true,
    unique: true,
		lowercase: true
  },
  encryptedPassword: {
    type: 'string',
    optional: true
  },
  isVerified: {
    type: 'boolean',
    default: false
  }
}, {
  versionKey: false
})

class User extends mongoose.model('User', userSchema) {

  static encrypt(string) {
    return new Promise(resolve => {
			bcrypt.hash(string, 10)
				.then(data => resolve(data))
    })
  }

  static createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY)
  } 
 
  static create(data) {
    return new Promise((resolve, reject) => {
      /* Destruct the data properties */
      let {
        username,
        email,
        password,
        password_confirmation
      } = data;

      if (password !== password_confirmation) throw new Error("Password and its confirmation doesn't match")

      let isValid = true;
      let validationObj = {}
      for (let props in this.schema.obj) {
        if (props === 'isVerified' || props === 'encryptedPassword') continue
        if (typeof data[props] !== this.schema.obj[props].type) {
          validationObj[props] = `${props} should have been ${this.schema.obj[props].type}`;
          isValid = false;
        }
      }

      if (!isValid) return reject({
        message: validationObj
      })

			this.encrypt(password) 
				.then(encryptedPassword => {
					return super.create({
						username,
						email,
						encryptedPassword
					})
				})
				.then(data => {
					let token = this.createToken({
						_id: data._id,
						isVerified: data.isVerified
					})

					resolve({
						_id: data._id,
						username: data.username,
						email: data.email,
						isVerified: data.isVerified,
						token: token
					})

					/* istanbul ignore next */
					if (process.env.NODE_ENV !== 'test') {
            let HTML = RegisterHTML({
              name: data.username,
              verificationLink: `${process.env.BASE_URL}/api/v1/auth/verify?verification_token=${token}`
            })

						mailer.send({
							to: data.email,
							from: 'mailer@moviedb.io',
							subject: 'Please verify your email!',
							html: HTML,
						})
							.catch(err => {
								console.log('Error:', err)
							})
					} 
				})
				.catch(err => {
					reject(err)
				})
    })
  }

  static authenticate(data) {
    return new Promise((resolve, reject) => {
			let {
				email,
				username,
				password
			} = data

			if (!email && !username || !username && !email) return reject({
				message: "Username or email is required"
			})

			if (!password) return reject({
				message: "Password is required"
			})

			let query = {}
			if (email) query.email = email.toLowerCase();
			if (username) query.username = username.toLowerCase();

      this.findOne(query).select(["_id","email", "username", "encryptedPassword", "isVerified"])
        .then(user => {
          if(!user) return reject({ message: "User doesn't seems to be existed!" })

          let isValid = bcrypt.compareSync(password, user.encryptedPassword)
          if (!isValid) return reject({ message: 'Incorrect password!'})

          let userEntity = {
            _id: user._id,
            username: user.username,
            email: user.email,
            isVerified: user.isVerified,
          }

          let token = this.createToken(userEntity)
          userEntity.token = token

          resolve(userEntity)
        })
    })
  }

  static verify(_id) {
    return new Promise((resolve, reject) => {
      this.findById(_id).then(async user => {
        if (!user)
          return reject({ message: 'User not found!' });

        if (user.isVerified)
          return resolve(`${user.username} is already verified`)

        user.isVerified = true; 
        await UserDetail.create({
          user_id: user._id,
        })

        return user.save()
      })
      .then(user => {
        resolve(`${user.username} is now verified`)
      })
      .catch(err => {
        reject(err)
      })
    })
  }

  static generateForgotPasswordToken(email) {
    return new Promise((resolve, reject) => {
      this
        .findOne({ email })
        .select(["_id", "isVerified"])
        .then(user => {
          if (!user) return reject({
            message: 'User not found!'
          })

          if (!user.isVerified) return reject({
            message: 'Please verify your email!'
          })

          let { _id } = user;
          let token = jwt.sign({ _id }, process.env.FORGOT_PASSWORD_SECRET)

          resolve(token)
        })
    })
  }

  static resetPassword({ _id, password, password_confirmation }) {
    return new Promise((resolve, reject) => {
      if (!password || !password_confirmation) return reject({
        message: 'password or password_confirmation is required'
      })

      if (password !== password_confirmation) return reject({
        message: "Password doesn't match"
      })

      this.findById(_id)
        .then(async user => {
          let encryptedPassword = await this.encrypt(password)
          user.encryptedPassword = encryptedPassword; 
          return user.save()
        })
        .then(user => {
          resolve("Password has changed!")
        })
    })
  }

  static findByIdWithDetail(_id) {
    return new Promise(resolve => {
      this
        .findById(_id)
        .select("-encryptedPassword")
        .then(async user => {
          let data = {
            _id: user._id,
            username: user.username,
            email: user.email,
            isVerified: user.isVerified,
          }

          if (user.isVerified) {
            data.detail = await UserDetail
              .findOne({
                user_id: _id
              })
              .select(["-user_id", '-_id'])
          }

          resolve(data)
        })
    })
  }

  static remove() {
    return this.deleteMany({})
  }
}

module.exports = User
