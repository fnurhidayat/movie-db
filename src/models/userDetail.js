const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userDetailSchema = new Schema({
  fullname: 'string',
  phone_number: 'string',
  address: 'string',
  image: 'string',
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
}, {
  versionKey: false,
  collection: 'user_details'
})

module.exports = mongoose.model('UserDetail', userDetailSchema)
