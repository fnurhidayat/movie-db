const mongoose = require('mongoose')
const ConnectionString = {
  development: 'mongodb://localhost/movie-db_development',
  test: process.env.DB_CONNECTION,
  staging: process.env.DB_CONNECTION,
  production: process.env.DB_CONNECTION,
}

/* istanbul ignore next */
const env = process.env.NODE_ENV || 'development';
mongoose.connect(ConnectionString[env],
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  }
)
  .catch(/* istanbul ignore next */ err => {
    console.error(err)
		if (env !== 'test')
			process.exit(1)
  })
