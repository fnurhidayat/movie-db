const router = require('express').Router()
const authenticate = require('../middlewares/authenticate.js')
const {
  register,
  login,
  verify,
  me,
  forgotPassword,
  resetPassword
} = require('../controllers/authController.js')

router.post('/register', register)
router.post('/login', login)
router.get('/verify', verify)
router.get('/me', authenticate.token, me)
router.post('/forgot-password', forgotPassword)
router.post('/reset-password', resetPassword)

module.exports = router;
