const router = require('express').Router()
const {
  update
} = require('../controllers/usersController.js')
const authenticate = require('../middlewares/authenticate.js')

router.put('/', authenticate.token, authenticate.isVerified, update)

module.exports = router;
