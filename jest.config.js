require('dotenv').config()

module.exports = {
  clearMocks: true,
  coverageDirectory: "coverage",
  testEnvironment: "node",
  testEnvironmentOptions: {
    DB_CONNECTION: process.env.DB_CONNECTION
  },
  testPathIgnorePatterns: [
    "/node_modules/",
    "/fixtures/"
  ] 
};
