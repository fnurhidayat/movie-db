const faker = require('faker')

module.exports = () => {
  return {
    fullname: faker.name.findName(),
    phone_number: faker.phone.phoneNumber(),
    address: faker.address.streetAddress() + ' ' + faker.address.city(),
    image: faker.image.imageUrl(),
  }
}
