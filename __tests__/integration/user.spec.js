const server = require('../index.spec.js')
const User = require('../../src/models/user.js')
const request = require('supertest')
const fixture = {
  user: require('../fixtures/user.js'),
  userDetail: require('../fixtures/userDetail.js')
}

describe('User Collection', () => {

  describe('PUT /api/v1/users', () => {
    let init = fixture.user()
    let sample;

    beforeAll(done => {
      User.create(init)
        .then(data => {
          sample = data
          done()
        })
    })

    afterAll(done => {
      User.remove().then(() => done())
    })

    test('Successfully update user detail', async done => {
      await User.verify(sample._id)
      let { token } = await User.authenticate({
        email: sample.email,
        password: init.password,
      })

      let body = fixture.userDetail()
      body = JSON.stringify(body)

      request(server)
        .put('/api/v1/users')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${token}`)
        .send(body)
        .expect(200)
        .end((_, res) => {
          body = JSON.parse(body)

          expect(res.body.status).toEqual(true)
          expect(res.body.data).toHaveProperty('fullname')
          expect(res.body.data).toHaveProperty('phone_number')
          expect(res.body.data).toHaveProperty('image')
          expect(res.body.data).toHaveProperty('address')

          User.findByIdWithDetail(sample._id)
            .then(data => {
              expect(data).toHaveProperty('detail')

              let {
                fullname,
                phone_number,
                image,
                address
              } = data.detail

              expect(fullname).toBe(body.fullname)
              expect(phone_number).toBe(body.phone_number)
              expect(image).toBe(body.image)
              expect(address).toBe(body.address)

              done()
            })
        })
    })

    test("Won't update user detail because email isn't verified", async done => {
      let init02 = fixture.user()

      let sample02 = await User.create(init02)

      let { token } = await User.authenticate({
        email: sample02.email,
        password: init02.password,
      })

      let body = fixture.userDetail()
      body = JSON.stringify(body)

      request(server)
        .put('/api/v1/users')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${token}`)
        .send(body)
        .expect(405)
        .end((_, res) => {
          expect(res.body.status).toEqual(false)
          expect(res.body.errors).toBe("You need to verify your account to do this action")
          done()
        })
    })
  })
})
