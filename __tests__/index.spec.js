const server = require('../src/index.js')
const request = require('supertest')
const faker = require('faker')

test('Root Endpoint', done => {
  request(server)
    .get('/')
    .expect(200)
    .end((_, res) => {
      expect(res.status).toEqual(200)
      expect(res.body.status).toEqual(true)
      expect(res.body.data.message).toEqual('Hello World')
      expect(res.body.data.environment).toEqual('test')
			done()
    })
})

test('404 Handler', done => {
  request(server)
    .get('/' + faker.lorem.words().trim())
    .expect(404)
    .end((_, res) => {
      expect(res.status).toEqual(404)
      expect(res.body.status).toEqual(false)
      expect(res.body.errors).toEqual('Not found!')
			done()
    })
})

test('API Router 404 Handler', done => {
  request(server)
    .get('/api/v1/notFound')
    .expect(404)
    .end((_, res) => {
      expect(res.status).toEqual(404)
      expect(res.body.status).toEqual(false)
      expect(res.body.errors).toEqual('Not found!')
			done()
    })
})

module.exports = server;
